((d, w, f) => {
    function videosA() {

    }
    w.addEventListener("load", function () {
        try {
            (() => {
                let tabsLinks = {};
                w.ytInitialData.contents.twoColumnBrowseResultsRenderer.tabs.forEach(tabData => {
                    if (!tabData.tabRenderer) {
                        return;
                    }
                    tabsLinks[tabData.tabRenderer.title.toUpperCase()] = tabData.tabRenderer.endpoint.commandMetadata.webCommandMetadata.url;
                });

                d.querySelectorAll("div.tab-content.style-scope.paper-tab").forEach((tabElement, href, tabCaption, a) => {
                    if (!(href = tabsLinks[tabCaption = tabElement.innerText.toUpperCase()])) {
                        return;
                    }

                    tabElement.innerText = '';

                    a = tabElement.createElement('a');
                    a.appendChild(tabElement.createTextNode(tabCaption));
                    a.href = href;

                    tabElement.appendChild(a);
                });
            })();
        } catch (e) {
            console.log(e);
        }
        (function () {
            function morph() {
                const aVideoTitles = d.querySelectorAll("a#video-title");
                aVideoTitles.forEach(function (aVideoTitle, v, a, u) {
                    u = new URL(aVideoTitle.href);
                    v = u.searchParams.get('v');
                    if (!u.searchParams.get('list')) {
                        return;
                    }
                    if (!v) {
                        return;
                    }
                    if (aVideoTitle.classList.contains(f)) {
                        return;
                    }
                    aVideoTitle.classList.add(f);
                    a = aVideoTitle.cloneNode(true);
                    a.href = "/watch?v=" + v;
                    a.target = "_blank";
                    aVideoTitle.innerHTML = "...";
                    aVideoTitle.parentNode.insertBefore(a, aVideoTitle);
                });
            }

            let morphAttemptCount = 0;

            function morphAttempt() {
                ++morphAttemptCount;
                let ytdBrowse = d.querySelector("ytd-browse");
                if (!ytdBrowse) {
                    setTimeout(morphAttempt, 0);
                    return;
                }
                console.log({morph: "success", morphAttemptCount})
                morph();
            }

            const ytdToolbarLogoRenderer = d.querySelector("ytd-topbar-logo-renderer#logo");

            //ytdToolbarLogoRenderer.innerHTML = '<div class="style-scope ytd-topbar-logo-renderer"><ytd-logo class="style-scope ytd-topbar-logo-renderer"><yt-icon id="logo-icon" class="style-scope ytd-logo"><yt-icon-shape class="style-scope yt-icon"> <icon-shape class="yt-spec-icon-shape"><div style="width: 100%; height: 100%; fill: currentcolor;"><svg class="external-icon" viewBox="0 0 90 20" focusable="false" style="pointer-events: none; display: block; width: 100%; height: 100%;"><svg viewBox="0 0 90 20" preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg"><g><path d="M27.9727 3.12324C27.6435 1.89323 26.6768 0.926623 25.4468 0.597366C23.2197 2.24288e-07 14.285 0 14.285 0C14.285 0 5.35042 2.24288e-07 3.12323 0.597366C1.89323 0.926623 0.926623 1.89323 0.597366 3.12324C2.24288e-07 5.35042 0 10 0 10C0 10 2.24288e-07 14.6496 0.597366 16.8768C0.926623 18.1068 1.89323 19.0734 3.12323 19.4026C5.35042 20 14.285 20 14.285 20C14.285 20 23.2197 20 25.4468 19.4026C26.6768 19.0734 27.6435 18.1068 27.9727 16.8768C28.5701 14.6496 28.5701 10 28.5701 10C28.5701 10 28.5677 5.35042 27.9727 3.12324Z" fill="#FF0000"></path><path d="M11.4253 14.2854L18.8477 10.0004L11.4253 5.71533V14.2854Z" fill="white"></path></g></svg></svg></div></icon-shape></yt-icon-shape></yt-icon></ytd-logo></div> ';
            ytdToolbarLogoRenderer.innerHTML = '                    <div style="width: 100%; height: 100%; fill: currentcolor;">\n' +
                '                        <svg class="external-icon" viewBox="0 0 90 20" focusable="false" style="pointer-events: none; display: block; width: 100%; height: 100%;">\n' +
                '                            <svg viewBox="0 0 90 20" preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg">\n' +
                '                                <g>\n' +
                '                                    <path d="M27.9727 3.12324C27.6435 1.89323 26.6768 0.926623 25.4468 0.597366C23.2197 2.24288e-07 14.285 0 14.285 0C14.285 0 5.35042 2.24288e-07 3.12323 0.597366C1.89323 0.926623 0.926623 1.89323 0.597366 3.12324C2.24288e-07 5.35042 0 10 0 10C0 10 2.24288e-07 14.6496 0.597366 16.8768C0.926623 18.1068 1.89323 19.0734 3.12323 19.4026C5.35042 20 14.285 20 14.285 20C14.285 20 23.2197 20 25.4468 19.4026C26.6768 19.0734 27.6435 18.1068 27.9727 16.8768C28.5701 14.6496 28.5701 10 28.5701 10C28.5701 10 28.5677 5.35042 27.9727 3.12324Z"\n' +
                '                                          fill="#FF0000"></path>\n' +
                '                                    <path d="M11.4253 14.2854L18.8477 10.0004L11.4253 5.71533V14.2854Z" fill="white"></path>\n' +
                '                                </g>\n' +
                '                            </svg>\n' +
                '                        </svg>\n' +
                '                    </div>\n';
            // ytdToolbarLogoRenderer.onclick = morph
            //
            // ytdToolbarLogoRenderer.style.backgroundColor = "white";
            // ytdToolbarLogoRenderer.style.textAlign = "center";
            // ytdToolbarLogoRenderer.style.fontWeight = "bold";
            // ytdToolbarLogoRenderer.style.fontSize = "2.2em";
            // ytdToolbarLogoRenderer.style.cursor = "pointer";

            morphAttempt();

        })();
    });

})(document, window, 'gfaiutgftagetafgjyeta');
