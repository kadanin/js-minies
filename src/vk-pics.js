((d, w, $$) => {
    $$ = (s) => d.querySelectorAll(s);
    $$(".page_post_sized_thumbs.clear_fix>a").forEach((a, w, h) => {
        w = d.createElement('div');
        w.appendChild(a.cloneNode(true));
        h      = d.createElement('a');
        h.href = h.innerText = ((h, t) => {
            t           = d.createElement('textarea');
            t.innerHTML = h;
            return (t.value.match('"x":"(https:.+.jpg)",')[1].split("\\").join(""));
        })(w.innerHTML);
        a.parentNode.appendChild(d.createElement('br'));
        a.parentNode.appendChild(h);
        a.parentNode.style.height = 'auto';
        console.log(h.href, h);
    })
})(document, window);
