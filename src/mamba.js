'use strict';

const divID = "fukken-div";

const div = document.createElement("div");
div.id = divID;
div.style.position = "fixed";
div.style.top = "10px";
div.style.left = "20px";
document.body.appendChild(div);

function _btn(text, callback) {
    const btn = document.createElement("button");
    btn.innerText = text;
    btn.onclick = callback;
    div.appendChild(btn);
}

_btn("RUN", run);
_btn("STOP", stop);

let _timeoutHandle = null;
const _timeout = 100;

/**
 *
 * @param selector
 * @returns {NodeListOf<HTMLElementTagNameMap[keyof HTMLElementTagNameMap]>}
 * @private
 */
function _querySelectorAll(selector) {
    return window.document.querySelectorAll(selector);
}

/**
 *
 * @param selector
 * @returns {HTMLElement | null}
 * @private
 */
function _querySelector(selector) {
    return window.document.querySelector(selector);
}

function run() {
    _timeoutHandle = setTimeout(_run, _timeout);

    function _run() {
    }

}

function stop() {
    if (null === _timeoutHandle) {
        return;
    }
    clearTimeout(_timeoutHandle);
    _timeoutHandle = null;
}
