((document, window) => {
    window.addEventListener("load", function () {
        let headerSpan = document.querySelector("h1>span");
        let header = headerSpan.parentNode;
        let a = document.createElement('a');
        a.href = document.documentURI;
        a.appendChild(headerSpan);
        header.appendChild(a);
    });

})(document, window);
