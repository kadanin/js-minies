#!/usr/bin/env php7.4
<?php

(new class {
    private ?string $fileName = null;
    private ?string $fileBaseName = null;

    public function run(): void
    {
        $input = $this->readFile();

        $minified = $this->request($input);

        $bytes = $this->save($minified);

        $this->o("Bytes Written: {$bytes}");
    }

    private function o(?string $string = null): void
    {
        $this->stdout($string . PHP_EOL);
    }

    private function stdout(string $string): void
    {
        \fwrite(\STDOUT, $string);
    }

    private function request(string $input): string
    {
        $url = 'https://www.toptal.com/developers/javascript-minifier/api/raw';

        $curlHandle = \curl_init();

        \curl_setopt_array($curlHandle, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => true,
            CURLOPT_HTTPHEADER     => ['Content-Type: application/x-www-form-urlencoded'],
            CURLOPT_POSTFIELDS     => \http_build_query(['input' => $input]),
        ]);

        $minified = \curl_exec($curlHandle);

        \curl_close($curlHandle);

        if (false === $minified) {
            throw new \RuntimeException('Request Fail');
        }

        return $minified;
    }

    private function readFile(): string
    {
        $fileName = $this->fileName();

        $result = \file_get_contents($fileName);
        if (false === $result) {
            throw new \RuntimeException("Cannot Read {$fileName}");
        }

        return $result;
    }

    private function fileName(): string
    {
        if (null !== $this->fileName) {
            return $this->fileName;
        }

        $this->initFileName();

        if (null === $this->fileName) {
            throw new \RuntimeException('Init Failed');
        }

        return $this->fileName;
    }

    private function fileBaseName(): string
    {
        if (null !== $this->fileBaseName) {
            return $this->fileBaseName;
        }

        $this->initFileName();

        if (null === $this->fileBaseName) {
            throw new \RuntimeException('Init Failed');
        }

        return $this->fileBaseName;
    }

    private function initFileName(): void
    {
        global $argv;

        $fileName = $argv[1] ?? null;
        if (null === $fileName) {
            throw new \RuntimeException('File Name Required');
        }

        $explodedFileName = \explode('.', $fileName);

        $ext = \array_pop($explodedFileName);

        if (('js' !== $ext) || ([] === $explodedFileName)) {
            throw new \RuntimeException("File Not JS File: {$fileName}");
        }

        $this->fileName = $fileName;
        $this->fileBaseName = \reset($explodedFileName);
    }

    private function save(string $minified): int
    {
        $result = file_put_contents($fileName = "{$this->fileBaseName()}.min.js", $minified);

        if (false === $result) {
            throw new RuntimeException("Cannot write {$fileName}");
        }

        return $result;
    }
})->run();