const path = require('path');

module.exports = {
    entry: {
        mamba: './src/mamba.js',
        youtube: './src/youtube.js',
        test: './src/test.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'build'),
    },
};
